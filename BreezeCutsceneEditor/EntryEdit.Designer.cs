﻿namespace BreezeCutsceneEditor
{
    partial class EntryEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EntryEdit));
            this.label1 = new System.Windows.Forms.Label();
            this.field_Message = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.field_ID = new System.Windows.Forms.NumericUpDown();
            this.field_SwitchID = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.field_Instant = new System.Windows.Forms.CheckBox();
            this.field_Location = new System.Windows.Forms.TextBox();
            this.field_Target = new System.Windows.Forms.TextBox();
            this.field_Character = new System.Windows.Forms.TextBox();
            this.field_Duration = new System.Windows.Forms.TextBox();
            this.field_Image = new System.Windows.Forms.TextBox();
            this.field_Speed = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.field_Extra = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.field_Flagged = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.field_Type = new System.Windows.Forms.TextBox();
            this.label_CommandType = new System.Windows.Forms.Label();
            this.field_Delete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.field_Save = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.field_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.field_SwitchID)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "ID";
            // 
            // field_Message
            // 
            this.field_Message.Location = new System.Drawing.Point(12, 123);
            this.field_Message.Name = "field_Message";
            this.field_Message.Size = new System.Drawing.Size(316, 71);
            this.field_Message.TabIndex = 3;
            this.field_Message.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Message";
            // 
            // field_ID
            // 
            this.field_ID.Location = new System.Drawing.Point(14, 74);
            this.field_ID.Margin = new System.Windows.Forms.Padding(2);
            this.field_ID.Name = "field_ID";
            this.field_ID.Size = new System.Drawing.Size(90, 20);
            this.field_ID.TabIndex = 5;
            // 
            // field_SwitchID
            // 
            this.field_SwitchID.Location = new System.Drawing.Point(128, 74);
            this.field_SwitchID.Margin = new System.Windows.Forms.Padding(2);
            this.field_SwitchID.Name = "field_SwitchID";
            this.field_SwitchID.Size = new System.Drawing.Size(90, 20);
            this.field_SwitchID.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(125, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "SwitchID";
            // 
            // field_Instant
            // 
            this.field_Instant.AutoSize = true;
            this.field_Instant.Location = new System.Drawing.Point(249, 78);
            this.field_Instant.Margin = new System.Windows.Forms.Padding(2);
            this.field_Instant.Name = "field_Instant";
            this.field_Instant.Size = new System.Drawing.Size(58, 17);
            this.field_Instant.TabIndex = 9;
            this.field_Instant.Text = "Instant";
            this.field_Instant.UseVisualStyleBackColor = true;
            // 
            // field_Location
            // 
            this.field_Location.Location = new System.Drawing.Point(8, 45);
            this.field_Location.Margin = new System.Windows.Forms.Padding(2);
            this.field_Location.Name = "field_Location";
            this.field_Location.Size = new System.Drawing.Size(91, 20);
            this.field_Location.TabIndex = 10;
            // 
            // field_Target
            // 
            this.field_Target.Location = new System.Drawing.Point(8, 98);
            this.field_Target.Margin = new System.Windows.Forms.Padding(2);
            this.field_Target.Name = "field_Target";
            this.field_Target.Size = new System.Drawing.Size(91, 20);
            this.field_Target.TabIndex = 11;
            // 
            // field_Character
            // 
            this.field_Character.Location = new System.Drawing.Point(122, 45);
            this.field_Character.Margin = new System.Windows.Forms.Padding(2);
            this.field_Character.Name = "field_Character";
            this.field_Character.Size = new System.Drawing.Size(91, 20);
            this.field_Character.TabIndex = 12;
            // 
            // field_Duration
            // 
            this.field_Duration.Location = new System.Drawing.Point(122, 98);
            this.field_Duration.Margin = new System.Windows.Forms.Padding(2);
            this.field_Duration.Name = "field_Duration";
            this.field_Duration.Size = new System.Drawing.Size(91, 20);
            this.field_Duration.TabIndex = 13;
            // 
            // field_Image
            // 
            this.field_Image.Location = new System.Drawing.Point(122, 148);
            this.field_Image.Margin = new System.Windows.Forms.Padding(2);
            this.field_Image.Name = "field_Image";
            this.field_Image.Size = new System.Drawing.Size(91, 20);
            this.field_Image.TabIndex = 14;
            // 
            // field_Speed
            // 
            this.field_Speed.Location = new System.Drawing.Point(8, 148);
            this.field_Speed.Margin = new System.Windows.Forms.Padding(2);
            this.field_Speed.Name = "field_Speed";
            this.field_Speed.Size = new System.Drawing.Size(91, 20);
            this.field_Speed.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Location";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Target";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Speed";
            // 
            // field_Extra
            // 
            this.field_Extra.Location = new System.Drawing.Point(237, 45);
            this.field_Extra.Margin = new System.Windows.Forms.Padding(2);
            this.field_Extra.Name = "field_Extra";
            this.field_Extra.Size = new System.Drawing.Size(91, 20);
            this.field_Extra.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(119, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Character";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(119, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Duration";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(119, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Image";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(235, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Extra";
            // 
            // field_Flagged
            // 
            this.field_Flagged.AutoSize = true;
            this.field_Flagged.Location = new System.Drawing.Point(249, 54);
            this.field_Flagged.Margin = new System.Windows.Forms.Padding(2);
            this.field_Flagged.Name = "field_Flagged";
            this.field_Flagged.Size = new System.Drawing.Size(64, 17);
            this.field_Flagged.TabIndex = 24;
            this.field_Flagged.Text = "Flagged";
            this.field_Flagged.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(235, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Type";
            // 
            // field_Type
            // 
            this.field_Type.Location = new System.Drawing.Point(237, 96);
            this.field_Type.Margin = new System.Windows.Forms.Padding(2);
            this.field_Type.Name = "field_Type";
            this.field_Type.Size = new System.Drawing.Size(91, 20);
            this.field_Type.TabIndex = 25;
            // 
            // label_CommandType
            // 
            this.label_CommandType.AutoSize = true;
            this.label_CommandType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CommandType.Location = new System.Drawing.Point(10, 13);
            this.label_CommandType.Name = "label_CommandType";
            this.label_CommandType.Size = new System.Drawing.Size(26, 20);
            this.label_CommandType.TabIndex = 27;
            this.label_CommandType.Text = "ID";
            // 
            // field_Delete
            // 
            this.field_Delete.Location = new System.Drawing.Point(205, 20);
            this.field_Delete.Margin = new System.Windows.Forms.Padding(2);
            this.field_Delete.Name = "field_Delete";
            this.field_Delete.Size = new System.Drawing.Size(155, 37);
            this.field_Delete.TabIndex = 28;
            this.field_Delete.Text = "Delete";
            this.field_Delete.UseVisualStyleBackColor = true;
            this.field_Delete.Click += new System.EventHandler(this.field_Delete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.field_Location);
            this.groupBox1.Controls.Add(this.field_Target);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.field_Character);
            this.groupBox1.Controls.Add(this.field_Type);
            this.groupBox1.Controls.Add(this.field_Duration);
            this.groupBox1.Controls.Add(this.field_Image);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.field_Speed);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.field_Extra);
            this.groupBox1.Location = new System.Drawing.Point(4, 281);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(378, 189);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Properties";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.field_Save);
            this.panel1.Controls.Add(this.field_Delete);
            this.panel1.Location = new System.Drawing.Point(-4, 476);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(394, 92);
            this.panel1.TabIndex = 30;
            // 
            // field_Save
            // 
            this.field_Save.Location = new System.Drawing.Point(32, 20);
            this.field_Save.Name = "field_Save";
            this.field_Save.Size = new System.Drawing.Size(158, 37);
            this.field_Save.TabIndex = 0;
            this.field_Save.Text = "Save";
            this.field_Save.UseVisualStyleBackColor = true;
            this.field_Save.Click += new System.EventHandler(this.field_Save_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.field_Message);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label_CommandType);
            this.panel2.Controls.Add(this.field_ID);
            this.panel2.Controls.Add(this.field_Flagged);
            this.panel2.Controls.Add(this.field_Instant);
            this.panel2.Controls.Add(this.field_SwitchID);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(-4, 65);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(394, 210);
            this.panel2.TabIndex = 31;
            // 
            // EntryEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 565);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(386, 650);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(386, 531);
            this.Name = "EntryEdit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Entry Info Screen";
            ((System.ComponentModel.ISupportInitialize)(this.field_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.field_SwitchID)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.RichTextBox field_Message;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.NumericUpDown field_ID;
        public System.Windows.Forms.NumericUpDown field_SwitchID;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.CheckBox field_Instant;
        public System.Windows.Forms.TextBox field_Location;
        public System.Windows.Forms.TextBox field_Target;
        public System.Windows.Forms.TextBox field_Character;
        public System.Windows.Forms.TextBox field_Duration;
        public System.Windows.Forms.TextBox field_Image;
        public System.Windows.Forms.TextBox field_Speed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox field_Extra;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.CheckBox field_Flagged;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox field_Type;
        private System.Windows.Forms.Label label_CommandType;
        private System.Windows.Forms.Button field_Delete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button field_Save;
    }
}