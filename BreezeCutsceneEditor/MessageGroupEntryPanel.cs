﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BreezeCutsceneEditor;
using MaterialSkin;
using MaterialSkin.Controls;


namespace BreezeCutsceneEditor
{
    public partial class MessageGroupEntryPanel : MaterialForm
    {
        #region

        //Container for Message Group Data
        [XmlArray("MessageGroups")]
        [XmlArrayItem("MessageGroup")]
        public List<MessageData> MessageGroup = new List<MessageData>();

        //This is the data of the message group
        public CommandData data = new CommandData();
        
        //Has a collection of messages that share the same GroupID
        public MessageGroupData messageGroupData = new MessageGroupData();

        public string FilePath = Path.Combine(Application.StartupPath + "\\MessageData.xml");

        public MainForm MainForm;
        #endregion

        public MessageGroupEntryPanel()
        {
            InitializeComponent();
            listView1.ListViewItemSorter = new ItemComparer();
            LoadCutsceneXmlInListView();

            LoadXML(FilePath);
        }

        //A message set with a list of "Lines"
        public MessageData CreateMessageData()
        {
            MessageData m = new MessageData();
            return m;
        }

        private void field_Save_Click(object sender, EventArgs e)
        {
            int id;
            var idResult = int.TryParse(field_ID.Text, out id);

            if (idResult)
                data.id = id;

            //TODO: figure out how to carry this from the next form
            //data.message = .Text;

            data.character = field_Character.Text.ToString();

            float duration;
            var durationResult = float.TryParse(field_Duration.Text, out duration);

            if (durationResult)
                data.duration = duration;

            data.extra = field_Extra.Text;
            data.image = field_Image.Text;
            data.target = field_Target.Text;

            SaveMessages(Path.Combine(Application.StartupPath, "MessageData.xml"));
            LoadXML(Path.Combine(Application.StartupPath, "MessageData.xml"));

            this.DialogResult = DialogResult.OK;
        }

        private void field_Delete_Click(object sender, EventArgs e)
        {
            MainForm.CommandData.Remove(data);
            MainForm.CutsceneData.CommandData = MainForm.CommandData;

            MainForm.Save(Path.Combine(Application.StartupPath, "MessageData.xml"));
            // Read the contents of testDialog's TextBox.
            //this.editForm.Text = testDialog.TextBox1.Text;
            MainForm.LoadXML(Path.Combine(Application.StartupPath, "MessageData.xml"));

            MainForm.LoadCutsceneXmlInListView();
            this.Dispose();
        }

        private void CreateLine_Click(object sender, EventArgs e)
        {

            GenerateLineForm();
        } 

        //Creates a form to modify Line text in
        private void GenerateLineForm()
        {
            var messageData = CreateMessageData();

            //Create a LineEdit window
            LineEdit editForm = new LineEdit();
            editForm.data = messageData;

            if (editForm.data.id == 0)
            {
                editForm.data.id = GetListViewMax() + 1;
            }


            //TODO: Implement proper nesting of Lines verus message groups
            //On Form Submit
            if (editForm.ShowDialog(this) == DialogResult.OK)
            {
                messageData = editForm.data;
                messageGroupData.Messages.Add(messageData);
                LoadCutsceneXmlInListView();

                //this.Dispose();
            }
            editForm.Dispose();
        }

        public void SaveMessages(string path)
        {
            var serializer = new XmlSerializer(typeof(MessageGroupData));

            using (var stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, this.messageGroupData);
                stream.Close();
            }
        }

        public MessageGroupData LoadXML(string path)
        {
            var serializer = new XmlSerializer(typeof(MessageGroupData));

            using (var stream = new FileStream(path, FileMode.Open))
            {
                try
                {
                    return serializer.Deserialize(stream) as MessageGroupData;
                }
                catch
                {
                    return null;
                }
            }
        }

        public void LoadCutsceneXmlInListView()
        {
            MessageGroup.Clear();
            messageGroupData = new MessageGroupData();
            messageGroupData.Messages.Clear();
            listView1.Items.Clear();
            messageGroupData = LoadXML(FilePath);

            try
            {
                foreach (var loadedData in messageGroupData.Messages)
                {
                    //Store the Item to the ListView
                    var item = listView1.Items.Add(loadedData.id.ToString());
                    
                    item.SubItems.Add(loadedData.message);
                    item.SubItems.Add(loadedData.id.ToString());

                    item.Tag = loadedData;
                    MessageGroup.Add(loadedData);
                }
            }
            catch
            {
                MessageBox.Show("Error: Unable to Process data file.");
            }

            ItemComparer sorter = listView1.ListViewItemSorter as ItemComparer;
            listView1.ListViewItemSorter = sorter;

            sorter.Column = 0;
            sorter.Order = System.Windows.Forms.SortOrder.Ascending;

            listView1.Sort();

        }

        public void ReorderItems(MessageData startingPoint)
        {
            var commandsToReorder = messageGroupData.Messages.Where(x => x.id >= startingPoint.id).OrderBy(x => x.id).ToList();
            commandsToReorder.Remove(startingPoint);

            var currentIndex = startingPoint.id + 1;

            foreach (var command in commandsToReorder)
            {
                command.id = currentIndex;
                currentIndex++;
            }
        }

        public int GetListViewMax()
        {
            List<int> Ids = new List<int>();

            foreach (ListViewItem e in listView1.Items)
            {
                Ids.Add(int.Parse(e.Text));
            }

            if (Ids.Count > 0)
                return Ids.Max();
            else
                return 0;
        }

        public class ItemComparer : IComparer
        {
            public int Column = 0;
            public System.Windows.Forms.SortOrder Order = SortOrder.Ascending;
            public int Compare(object x, object y) // IComparer Member
            {
                if (!(x is ListViewItem))
                    return (0);

                if (!(y is ListViewItem))
                    return (0);

                ListViewItem l1 = (ListViewItem)x;
                ListViewItem l2 = (ListViewItem)y;


                float fl1 = float.Parse(l1.SubItems[Column].Text);
                float fl2 = float.Parse(l2.SubItems[Column].Text);

                if (Order == SortOrder.Ascending)
                {
                    return fl1.CompareTo(fl2);
                }
                else
                {
                    return fl2.CompareTo(fl1);
                }
            }
        }

    }
}
