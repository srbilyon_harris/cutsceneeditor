﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace BreezeCutsceneEditor
{
    public partial class CommandPanel : MaterialForm
    {
        public MainForm MainForm;

        public CommandPanel()
        {
            InitializeComponent();
        }

        public CommandData CreateEventData(string commandType)
        {
            CommandData c = new CommandData();
            c.CommandType = commandType;
            return c;
        }

        public void Save(string path)
        {
            var serializer = new XmlSerializer(typeof(CutsceneData));

            using (var stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, MainForm.CutsceneData);
                stream.Close();
            }
        }

        public void SaveMessage(string path)
        {
            var serializer = new XmlSerializer(typeof(CutsceneData));

            using (var stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, MainForm.CutsceneData);
                stream.Close();
            }
        }

        public CutsceneData LoadXML(string path)
        {
            var serializer = new XmlSerializer(typeof(CutsceneData));

            using (var stream = new FileStream(path, FileMode.Open))
            {
                return serializer.Deserialize(stream) as CutsceneData;
            }
        }

        private void GenerateCommandForm(string CommandType)
        {
            var commandData = CreateEventData(CommandType);

            EntryEdit editForm = new EntryEdit();
            editForm.data = commandData;

            if (editForm.data.id == 0)
            {
                editForm.data.id = MainForm.GetListViewMax() + 1;
            }

            editForm.GetIdTextBox().Text = editForm.data.id.ToString();
            editForm.GetMessageTextBox().Text = editForm.data.message;
            
            //this.Hide();

            if (editForm.ShowDialog(this) == DialogResult.OK)
            {
                commandData = editForm.data;
                MainForm.CutsceneData.CommandData.Add(commandData);
                MainForm.ReorderItems(commandData);

                Save(Path.Combine(Application.StartupPath, "Cutscene1.xml"));
                LoadXML(Path.Combine(Application.StartupPath, "Cutscene1.xml"));

                MainForm.LoadCutsceneXmlInListView();

                this.Dispose();
            }
            editForm.Dispose();
        }

        private void GenerateMessageForm(string CommandType)
        {
            var commandData = CreateEventData(CommandType);

            MessageGroupEntryPanel editForm = new MessageGroupEntryPanel();
            editForm.data = commandData;

            if (editForm.data.id == 0)
            {
                editForm.data.id = MainForm.GetListViewMax() + 1;
            }

            //editForm.GetIdTextBox().Text = editForm.data.id.ToString();
            //editForm.GetMessageTextBox().Text = editForm.data.message;

            //this.Hide();


            if (editForm.ShowDialog(this) == DialogResult.OK)
            {
                commandData = editForm.data;
                MainForm.CutsceneData.CommandData.Add(commandData);
                MainForm.ReorderItems(commandData);

                Save(Path.Combine(Application.StartupPath, "Cutscene1.xml"));
                LoadXML(Path.Combine(Application.StartupPath, "Cutscene1.xml"));

                MainForm.LoadCutsceneXmlInListView();

                //this.Dispose();
            }
            editForm.Dispose();
        }

        private void CreateSpeechBox_Click(object sender, EventArgs e)
        {
           GenerateCommandForm("CreateSpeechBox");
        }

        private void ChangeScene_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("ChangeScene");
        }

        private void ChangeCameraTaret_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("ChangeCameraTarget");
        }

        private void MoveToPoint_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("MoveToPoint");
        }

        private void CreateOverheadPrompt_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("CreateOverheadPrompt");
        }

        private void ChangeSwitch_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("ChangeSwitch");
        }

        private void PanCamera_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("PanCamera");
        }

        private void MoveToObject_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("MoveToObject");
        }

        private void PlayAnimation_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("PlayAnimation");
        }
        private void Wait_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("Wait");
        }

        private void TrackCamera_Click(object sender, EventArgs e)
        {
            GenerateCommandForm("TrackCamera");
        }

        private void ShowMessage_Click(object sender, EventArgs e)
        {
            GenerateMessageForm("ShowMessage");
        }
    }
}
