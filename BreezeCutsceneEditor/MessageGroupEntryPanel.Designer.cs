﻿namespace BreezeCutsceneEditor
{
    partial class MessageGroupEntryPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Message = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.field_ID = new System.Windows.Forms.NumericUpDown();
            this.field_Image = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.field_Target = new System.Windows.Forms.TextBox();
            this.field_Duration = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.field_Extra = new System.Windows.Forms.TextBox();
            this.field_Save = new System.Windows.Forms.Button();
            this.field_Delete = new System.Windows.Forms.Button();
            this.field_Character = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CreateLine = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.field_ID)).BeginInit();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.listView1.AllowColumnReorder = true;
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.Message});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(12, 78);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.ShowGroups = false;
            this.listView1.Size = new System.Drawing.Size(931, 378);
            this.listView1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 106;
            // 
            // Message
            // 
            this.Message.Text = "Message";
            this.Message.Width = 441;
            // 
            // field_ID
            // 
            this.field_ID.Location = new System.Drawing.Point(12, 489);
            this.field_ID.Margin = new System.Windows.Forms.Padding(2);
            this.field_ID.Name = "field_ID";
            this.field_ID.Size = new System.Drawing.Size(90, 20);
            this.field_ID.TabIndex = 25;
            // 
            // field_Image
            // 
            this.field_Image.Location = new System.Drawing.Point(333, 490);
            this.field_Image.Margin = new System.Windows.Forms.Padding(2);
            this.field_Image.Name = "field_Image";
            this.field_Image.Size = new System.Drawing.Size(91, 20);
            this.field_Image.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(330, 472);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Image";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 472);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "ID";
            // 
            // field_Target
            // 
            this.field_Target.Location = new System.Drawing.Point(117, 490);
            this.field_Target.Margin = new System.Windows.Forms.Padding(2);
            this.field_Target.Name = "field_Target";
            this.field_Target.Size = new System.Drawing.Size(91, 20);
            this.field_Target.TabIndex = 26;
            // 
            // field_Duration
            // 
            this.field_Duration.Location = new System.Drawing.Point(225, 490);
            this.field_Duration.Margin = new System.Windows.Forms.Padding(2);
            this.field_Duration.Name = "field_Duration";
            this.field_Duration.Size = new System.Drawing.Size(91, 20);
            this.field_Duration.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(447, 472);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "Extra";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(117, 472);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Target";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(222, 472);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Duration";
            // 
            // field_Extra
            // 
            this.field_Extra.Location = new System.Drawing.Point(449, 489);
            this.field_Extra.Margin = new System.Windows.Forms.Padding(2);
            this.field_Extra.Name = "field_Extra";
            this.field_Extra.Size = new System.Drawing.Size(91, 20);
            this.field_Extra.TabIndex = 30;
            // 
            // field_Save
            // 
            this.field_Save.Location = new System.Drawing.Point(186, 541);
            this.field_Save.Name = "field_Save";
            this.field_Save.Size = new System.Drawing.Size(158, 37);
            this.field_Save.TabIndex = 34;
            this.field_Save.Text = "Save";
            this.field_Save.UseVisualStyleBackColor = true;
            this.field_Save.Click += new System.EventHandler(this.field_Save_Click);
            // 
            // field_Delete
            // 
            this.field_Delete.Location = new System.Drawing.Point(359, 541);
            this.field_Delete.Margin = new System.Windows.Forms.Padding(2);
            this.field_Delete.Name = "field_Delete";
            this.field_Delete.Size = new System.Drawing.Size(155, 37);
            this.field_Delete.TabIndex = 35;
            this.field_Delete.Text = "Delete";
            this.field_Delete.UseVisualStyleBackColor = true;
            this.field_Delete.Click += new System.EventHandler(this.field_Delete_Click);
            // 
            // field_Character
            // 
            this.field_Character.Location = new System.Drawing.Point(555, 489);
            this.field_Character.Margin = new System.Windows.Forms.Padding(2);
            this.field_Character.Name = "field_Character";
            this.field_Character.Size = new System.Drawing.Size(91, 20);
            this.field_Character.TabIndex = 36;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(552, 472);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "Character";
            // 
            // CreateLine
            // 
            this.CreateLine.Location = new System.Drawing.Point(15, 541);
            this.CreateLine.Margin = new System.Windows.Forms.Padding(2);
            this.CreateLine.Name = "CreateLine";
            this.CreateLine.Size = new System.Drawing.Size(166, 37);
            this.CreateLine.TabIndex = 38;
            this.CreateLine.Text = "Create Line";
            this.CreateLine.UseVisualStyleBackColor = true;
            this.CreateLine.Click += new System.EventHandler(this.CreateLine_Click);
            // 
            // MessageGroupEntryPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 608);
            this.Controls.Add(this.CreateLine);
            this.Controls.Add(this.field_Character);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.field_Save);
            this.Controls.Add(this.field_Delete);
            this.Controls.Add(this.field_ID);
            this.Controls.Add(this.field_Image);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.field_Target);
            this.Controls.Add(this.field_Duration);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.field_Extra);
            this.Controls.Add(this.listView1);
            this.Name = "MessageGroupEntryPanel";
            this.Text = "Message Entry";
            ((System.ComponentModel.ISupportInitialize)(this.field_ID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader Message;
        public System.Windows.Forms.NumericUpDown field_ID;
        public System.Windows.Forms.TextBox field_Image;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox field_Target;
        public System.Windows.Forms.TextBox field_Duration;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox field_Extra;
        private System.Windows.Forms.Button field_Save;
        private System.Windows.Forms.Button field_Delete;
        public System.Windows.Forms.TextBox field_Character;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button CreateLine;
    }
}