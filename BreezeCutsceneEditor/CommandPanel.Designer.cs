﻿namespace BreezeCutsceneEditor
{
    partial class CommandPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommandPanel));
            this.CreateSpeechBox = new System.Windows.Forms.Button();
            this.CreateOverheadPrompt = new System.Windows.Forms.Button();
            this.ChangeSwitch = new System.Windows.Forms.Button();
            this.ChangeScene = new System.Windows.Forms.Button();
            this.MoveToObject = new System.Windows.Forms.Button();
            this.MoveToPoint = new System.Windows.Forms.Button();
            this.PanCamera = new System.Windows.Forms.Button();
            this.ChangeCameraTaret = new System.Windows.Forms.Button();
            this.Wait = new System.Windows.Forms.Button();
            this.TrackCamera = new System.Windows.Forms.Button();
            this.PlayAnimation = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ShowMessage = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // CreateSpeechBox
            // 
            this.CreateSpeechBox.Location = new System.Drawing.Point(10, 104);
            this.CreateSpeechBox.Margin = new System.Windows.Forms.Padding(2);
            this.CreateSpeechBox.Name = "CreateSpeechBox";
            this.CreateSpeechBox.Size = new System.Drawing.Size(130, 39);
            this.CreateSpeechBox.TabIndex = 0;
            this.CreateSpeechBox.Text = "Create Speech Box";
            this.CreateSpeechBox.UseVisualStyleBackColor = true;
            this.CreateSpeechBox.Click += new System.EventHandler(this.CreateSpeechBox_Click);
            // 
            // CreateOverheadPrompt
            // 
            this.CreateOverheadPrompt.Location = new System.Drawing.Point(5, 149);
            this.CreateOverheadPrompt.Margin = new System.Windows.Forms.Padding(2);
            this.CreateOverheadPrompt.Name = "CreateOverheadPrompt";
            this.CreateOverheadPrompt.Size = new System.Drawing.Size(130, 39);
            this.CreateOverheadPrompt.TabIndex = 1;
            this.CreateOverheadPrompt.Text = "Create Overhead Prompt";
            this.CreateOverheadPrompt.UseVisualStyleBackColor = true;
            this.CreateOverheadPrompt.Click += new System.EventHandler(this.CreateOverheadPrompt_Click);
            // 
            // ChangeSwitch
            // 
            this.ChangeSwitch.Location = new System.Drawing.Point(10, 61);
            this.ChangeSwitch.Margin = new System.Windows.Forms.Padding(2);
            this.ChangeSwitch.Name = "ChangeSwitch";
            this.ChangeSwitch.Size = new System.Drawing.Size(130, 39);
            this.ChangeSwitch.TabIndex = 3;
            this.ChangeSwitch.Text = "Change Switch";
            this.ChangeSwitch.UseVisualStyleBackColor = true;
            this.ChangeSwitch.Click += new System.EventHandler(this.ChangeSwitch_Click);
            // 
            // ChangeScene
            // 
            this.ChangeScene.Location = new System.Drawing.Point(10, 18);
            this.ChangeScene.Margin = new System.Windows.Forms.Padding(2);
            this.ChangeScene.Name = "ChangeScene";
            this.ChangeScene.Size = new System.Drawing.Size(130, 39);
            this.ChangeScene.TabIndex = 2;
            this.ChangeScene.Text = "Change Scene";
            this.ChangeScene.UseVisualStyleBackColor = true;
            this.ChangeScene.Click += new System.EventHandler(this.ChangeScene_Click);
            // 
            // MoveToObject
            // 
            this.MoveToObject.Location = new System.Drawing.Point(5, 106);
            this.MoveToObject.Margin = new System.Windows.Forms.Padding(2);
            this.MoveToObject.Name = "MoveToObject";
            this.MoveToObject.Size = new System.Drawing.Size(130, 39);
            this.MoveToObject.TabIndex = 7;
            this.MoveToObject.Text = "Move To Object";
            this.MoveToObject.UseVisualStyleBackColor = true;
            this.MoveToObject.Click += new System.EventHandler(this.MoveToObject_Click);
            // 
            // MoveToPoint
            // 
            this.MoveToPoint.Location = new System.Drawing.Point(5, 63);
            this.MoveToPoint.Margin = new System.Windows.Forms.Padding(2);
            this.MoveToPoint.Name = "MoveToPoint";
            this.MoveToPoint.Size = new System.Drawing.Size(130, 39);
            this.MoveToPoint.TabIndex = 6;
            this.MoveToPoint.Text = "Move To Point";
            this.MoveToPoint.UseVisualStyleBackColor = true;
            this.MoveToPoint.Click += new System.EventHandler(this.MoveToPoint_Click);
            // 
            // PanCamera
            // 
            this.PanCamera.Location = new System.Drawing.Point(5, 64);
            this.PanCamera.Margin = new System.Windows.Forms.Padding(2);
            this.PanCamera.Name = "PanCamera";
            this.PanCamera.Size = new System.Drawing.Size(130, 39);
            this.PanCamera.TabIndex = 5;
            this.PanCamera.Text = "Pan Camera";
            this.PanCamera.UseVisualStyleBackColor = true;
            this.PanCamera.Click += new System.EventHandler(this.PanCamera_Click);
            // 
            // ChangeCameraTaret
            // 
            this.ChangeCameraTaret.Location = new System.Drawing.Point(6, 107);
            this.ChangeCameraTaret.Margin = new System.Windows.Forms.Padding(2);
            this.ChangeCameraTaret.Name = "ChangeCameraTaret";
            this.ChangeCameraTaret.Size = new System.Drawing.Size(130, 39);
            this.ChangeCameraTaret.TabIndex = 4;
            this.ChangeCameraTaret.Text = "Change Camera Target";
            this.ChangeCameraTaret.UseVisualStyleBackColor = true;
            this.ChangeCameraTaret.Click += new System.EventHandler(this.ChangeCameraTaret_Click);
            // 
            // Wait
            // 
            this.Wait.Location = new System.Drawing.Point(10, 190);
            this.Wait.Margin = new System.Windows.Forms.Padding(2);
            this.Wait.Name = "Wait";
            this.Wait.Size = new System.Drawing.Size(130, 39);
            this.Wait.TabIndex = 9;
            this.Wait.Text = "Wait";
            this.Wait.UseVisualStyleBackColor = true;
            this.Wait.Click += new System.EventHandler(this.Wait_Click);
            // 
            // TrackCamera
            // 
            this.TrackCamera.Location = new System.Drawing.Point(5, 21);
            this.TrackCamera.Margin = new System.Windows.Forms.Padding(2);
            this.TrackCamera.Name = "TrackCamera";
            this.TrackCamera.Size = new System.Drawing.Size(130, 39);
            this.TrackCamera.TabIndex = 8;
            this.TrackCamera.Text = "Track Camera";
            this.TrackCamera.UseVisualStyleBackColor = true;
            this.TrackCamera.Click += new System.EventHandler(this.TrackCamera_Click);
            // 
            // PlayAnimation
            // 
            this.PlayAnimation.Location = new System.Drawing.Point(5, 20);
            this.PlayAnimation.Margin = new System.Windows.Forms.Padding(2);
            this.PlayAnimation.Name = "PlayAnimation";
            this.PlayAnimation.Size = new System.Drawing.Size(130, 39);
            this.PlayAnimation.TabIndex = 10;
            this.PlayAnimation.Text = "Play Animation";
            this.PlayAnimation.UseVisualStyleBackColor = true;
            this.PlayAnimation.Click += new System.EventHandler(this.PlayAnimation_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.TrackCamera);
            this.groupBox1.Controls.Add(this.PanCamera);
            this.groupBox1.Controls.Add(this.ChangeCameraTaret);
            this.groupBox1.Location = new System.Drawing.Point(163, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(141, 307);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Camera";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.PlayAnimation);
            this.groupBox2.Controls.Add(this.MoveToPoint);
            this.groupBox2.Controls.Add(this.MoveToObject);
            this.groupBox2.Controls.Add(this.CreateOverheadPrompt);
            this.groupBox2.Location = new System.Drawing.Point(310, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(140, 306);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Actor Control";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.ShowMessage);
            this.groupBox3.Controls.Add(this.ChangeScene);
            this.groupBox3.Controls.Add(this.CreateSpeechBox);
            this.groupBox3.Controls.Add(this.ChangeSwitch);
            this.groupBox3.Controls.Add(this.Wait);
            this.groupBox3.Location = new System.Drawing.Point(12, 76);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(145, 308);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Global";
            // 
            // ShowMessage
            // 
            this.ShowMessage.Location = new System.Drawing.Point(10, 147);
            this.ShowMessage.Margin = new System.Windows.Forms.Padding(2);
            this.ShowMessage.Name = "ShowMessage";
            this.ShowMessage.Size = new System.Drawing.Size(130, 39);
            this.ShowMessage.TabIndex = 10;
            this.ShowMessage.Text = "Show Message";
            this.ShowMessage.UseVisualStyleBackColor = true;
            this.ShowMessage.Click += new System.EventHandler(this.ShowMessage_Click);
            // 
            // CommandPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 396);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CommandPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Event Commands";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CreateSpeechBox;
        private System.Windows.Forms.Button CreateOverheadPrompt;
        private System.Windows.Forms.Button ChangeSwitch;
        private System.Windows.Forms.Button ChangeScene;
        private System.Windows.Forms.Button MoveToObject;
        private System.Windows.Forms.Button MoveToPoint;
        private System.Windows.Forms.Button PanCamera;
        private System.Windows.Forms.Button ChangeCameraTaret;
        private System.Windows.Forms.Button Wait;
        private System.Windows.Forms.Button TrackCamera;
        private System.Windows.Forms.Button PlayAnimation;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button ShowMessage;
    }
}