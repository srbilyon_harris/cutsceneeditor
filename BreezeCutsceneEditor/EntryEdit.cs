﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreezeCutsceneEditor
{
    public partial class EntryEdit : MaterialForm
    {
        public CommandData data = new CommandData();
        public MainForm MainForm;

        public EntryEdit()
        {
            InitializeComponent();
        }

        public NumericUpDown GetIdTextBox()
        {
            label_CommandType.Text = data.CommandType;

            return field_ID;
        }

        public RichTextBox GetMessageTextBox()
        {
            return field_Message;
        }

        private void field_Save_Click(object sender, EventArgs e)
        {
            int id;
            var idResult = int.TryParse(field_ID.Text, out id);
            if (idResult)
                data.id = id;

            data.message = field_Message.Text;
            data.character = field_Character.Text.ToString();

            float duration;
            var durationResult = float.TryParse(field_Duration.Text, out duration);
            if (durationResult)
                data.duration = duration;

            data.extra = field_Extra.Text;
            data.flag = field_Flagged.Checked;
            data.image = field_Image.Text;
            data.instant = field_Instant.Checked;
            data.location = field_Location.Text;

            float speed;
            var speedResult = float.TryParse(field_Speed.Text, out speed);
            if (speedResult)
                data.speed = speed;

            int switchID;
            var switchIDResult = int.TryParse(field_SwitchID.Text, out switchID);
            if (switchIDResult)
                data.switchID = switchID;

            data.target = field_Target.Text;
            data.type = field_Type.Text;

            this.DialogResult = DialogResult.OK;
        }

        private void field_Delete_Click(object sender, EventArgs e)
        {
            MainForm.CommandData.Remove(data);
            MainForm.CutsceneData.CommandData = MainForm.CommandData;

            MainForm.Save(Path.Combine(Application.StartupPath, "Cutscene1.xml"));
            // Read the contents of testDialog's TextBox.
            //this.editForm.Text = testDialog.TextBox1.Text;
            MainForm.LoadXML(Path.Combine(Application.StartupPath, "Cutscene1.xml"));

            MainForm.LoadCutsceneXmlInListView();
            this.Dispose();
        }
    }
}
