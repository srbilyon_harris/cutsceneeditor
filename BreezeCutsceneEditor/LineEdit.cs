﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreezeCutsceneEditor
{
    public partial class LineEdit : MaterialForm
    {
        public MessageData data = new MessageData();
        public MessageGroupEntryPanel form;
        public string message;


        public LineEdit()
        {
            InitializeComponent();
        }

        public RichTextBox GetMessageTextBox()
        {
            return richTextBox1;
        }


        private void saveButton_Click(object sender, EventArgs e)
        {
            data.message = GetMessageTextBox().Text;

            //Create a list of the current data lines
            //List<MessageData> lines = data.lines;

            //Generate a new line of text to add
            //LineData newline = new LineData();

            //Define content of new line
            //newline.line = GetMessageTextBox().Text;

            //Update the id of the new line
            //newline.lineID = lines.Count + 1;

            //add that new line to the list
            //lines.Add(newline);

            //Update the list
            //data.lines = lines;
                  
            this.DialogResult = DialogResult.OK;
        }
    }
}
