﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BreezeCutsceneEditor;
using MaterialSkin;
using MaterialSkin.Controls;

namespace BreezeCutsceneEditor
{
    public partial class MainForm : MaterialForm
    {
        #region Members
        //Container for Command Data
        [XmlArray("Commands")]
        [XmlArrayItem("Command")]
        public List<CommandData> CommandData = new List<CommandData>();

  

        public CutsceneData CutsceneData = new CutsceneData();
        public MessageGroupData MessageGroupData = new MessageGroupData();

        public string FilePath = Path.Combine(Application.StartupPath + "\\Cutscene1.xml");
        #endregion

        public MainForm()
        {
            InitializeComponent();

            listView1.ListViewItemSorter = new ItemComparer();

            //Event listener to check for a double click
            listView1.MouseDoubleClick += new MouseEventHandler(listView1_MouseDoubleClick);

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
        }

        #region IO
        public void Save(string path)
        {
            //ProcessData();

            var serializer = new XmlSerializer(typeof(CutsceneData));

            using (var stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, this.CutsceneData);
                stream.Close();
            }
        } 

        public CutsceneData LoadXML(string path)
        {
            var serializer = new XmlSerializer(typeof(CutsceneData));

            using (var stream = new FileStream(path, FileMode.Open))
            {
                try
                {
                    return serializer.Deserialize(stream) as CutsceneData;
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Open Dialog to load file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void field_LoadFromFile_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;

                try
                {
                    FilePath = file;
                    LoadCutsceneXmlInListView();
                }
                catch (IOException)
                {
                    MessageBox.Show("Error: Unable to Load data file.");
                }
            }
        }

        #endregion

        private void LoadData_Click(object sender, EventArgs e)
        {
            LoadCutsceneXmlInListView();
        }

        //Load the Cutscene data into it's list view
        public void LoadCutsceneXmlInListView()
        {
            CommandData.Clear();
            CutsceneData = new CutsceneData();
            CutsceneData.CommandData.Clear();
            listView1.Items.Clear();
            CutsceneData = LoadXML(FilePath);

            try
            {
                foreach (var loadedData in CutsceneData.CommandData)
                {
                    //Store the Item to the ListView
                    var item = listView1.Items.Add(loadedData.id.ToString());

                    //Store the ID to use as an index
                    item.SubItems.Add(loadedData.CommandType.ToString());
                    item.SubItems.Add(loadedData.message.ToString());
                    item.SubItems.Add(loadedData.character);
                    item.SubItems.Add(loadedData.extra.ToString());
                    item.SubItems.Add(loadedData.location.ToString());
                    item.SubItems.Add(loadedData.image.ToString());
                    item.SubItems.Add(loadedData.target.ToString());
                    item.SubItems.Add(loadedData.type.ToString());
                    item.SubItems.Add(loadedData.duration.ToString());
                    item.SubItems.Add(loadedData.flag.ToString());
                    item.SubItems.Add(loadedData.instant.ToString());
                    item.SubItems.Add(loadedData.speed.ToString());
                    item.SubItems.Add(loadedData.switchID.ToString());

                    item.Tag = loadedData;
                    CommandData.Add(loadedData);
                }
            }
            catch
            {
                MessageBox.Show("Error: Unable to Process data file.");
            }

            ItemComparer sorter = listView1.ListViewItemSorter as ItemComparer;
            listView1.ListViewItemSorter = sorter;

            sorter.Column = 0;
            sorter.Order = System.Windows.Forms.SortOrder.Ascending;

            listView1.Sort();

        }

        public void ReorderItems(CommandData startingPoint)
        {
            var commandsToReorder = CutsceneData.CommandData.Where(x => x.id >= startingPoint.id).OrderBy(x => x.id).ToList();
            commandsToReorder.Remove(startingPoint);

            var currentIndex = startingPoint.id + 1;

            foreach (var command in commandsToReorder)
            {
                command.id = currentIndex;
                currentIndex++;
            }
        }

        public int GetListViewMax()
        {
            List<int> Ids = new List<int>();

            foreach (ListViewItem e in listView1.Items)
            {
                Ids.Add(int.Parse(e.Text));
            }

            if (Ids.Count > 0)
                return Ids.Max();
            else
                return 0;
        }

        public class ItemComparer : IComparer
        {
            public int Column = 0;
            public System.Windows.Forms.SortOrder Order = SortOrder.Ascending;
            public int Compare(object x, object y) // IComparer Member
            {
                if (!(x is ListViewItem))
                    return (0);

                if (!(y is ListViewItem))
                    return (0);

                ListViewItem l1 = (ListViewItem)x;
                ListViewItem l2 = (ListViewItem)y;


                float fl1 = float.Parse(l1.SubItems[Column].Text);
                float fl2 = float.Parse(l2.SubItems[Column].Text);

                if (Order == SortOrder.Ascending)
                {
                    return fl1.CompareTo(fl2);
                }
                else
                {
                    return fl2.CompareTo(fl1);
                }
            }
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //Grab the cutscene entry from the list view
            ListViewHitTestInfo info = listView1.HitTest(e.X, e.Y);

            //Turn it into a List View Item
            ListViewItem item = info.Item;

            //Create a temporary model to store the data for the xml entry
            CommandData commandData = (CommandData)item.Tag;

            if (item != null)
            {
                EntryEdit editForm = new EntryEdit();
                editForm.data = commandData;
                editForm.MainForm = this;

                editForm.GetIdTextBox().Text = editForm.data.id.ToString();
                editForm.GetMessageTextBox().Text = editForm.data.message;


                editForm.field_ID.Text = editForm.data.id.ToString();
                editForm.field_Message.Text = editForm.data.message;
                editForm.field_Character.Text = editForm.data.character;

                editForm.field_Duration.Text = editForm.data.duration.ToString();
                editForm.field_Extra.Text = editForm.data.extra;

                editForm.field_Flagged.Checked = editForm.data.flag;
                editForm.field_Image.Text = editForm.data.image;

                editForm.field_Instant.Checked = editForm.data.instant;

                editForm.field_Location.Text = editForm.data.location;
                editForm.field_Speed.Text = editForm.data.speed.ToString();

                editForm.field_SwitchID.Text = editForm.data.switchID.ToString();
                editForm.field_Target.Text = editForm.data.target;
                editForm.field_Type.Text = editForm.data.type;

                // Show testDialog as a modal dialog and determine if DialogResult = OK.
                if (editForm.ShowDialog(this) == DialogResult.OK)
                {
                    commandData = editForm.data;
                    ReorderItems(commandData);

                    Save(FilePath);
                    // Read the contents of testDialog's TextBox.
                    //this.editForm.Text = testDialog.TextBox1.Text;
                    LoadXML(FilePath);

                    LoadCutsceneXmlInListView();
                }

                editForm.Dispose();
                //MessageBox.Show("The selected Item ID is: " + commandData.id);
            }
            else
            {
                this.listView1.SelectedItems.Clear();
                MessageBox.Show("No Item is selected");
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CreateCommand_Click(object sender, EventArgs e)
        {
            CommandPanel commandPanel = new CommandPanel();

            commandPanel.MainForm = this;
            commandPanel.ShowDialog();
        }


        private void MainForm_Load(object sender, EventArgs e)
        {

        }

    }
}
