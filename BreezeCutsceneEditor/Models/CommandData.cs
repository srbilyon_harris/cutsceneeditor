﻿using System;
using System.Xml;
using System.Xml.Serialization;


namespace BreezeCutsceneEditor
{
    [System.Serializable]
    public class CommandData
    {
        /// <summary>
        /// Used to determine what type of command this is
        /// </summary>
        [XmlAttribute("CommandType")]
        public string CommandType;

        /// <summary>
        /// Used for determining event order
        /// </summary>
        [XmlAttribute("id")]
        public int id;

        /// <summary>
        /// Used for updating the "Flagged" parameter for an event
        /// </summary>
        [XmlAttribute("flag")] public bool flag = false;

        /// <summary>
        /// Used for designate that this event should be completed immediately
        /// </summary>
        [XmlAttribute("instant")] public bool instant = false;

        /// <summary>
        /// Used for selecting a game switch
        /// </summary>
        [XmlAttribute("switchID")]
        public int switchID = -1;

        /// <summary>
        /// Used to designate a location
        /// </summary>
        [XmlAttribute("location")]
        public string location = String.Empty;

        /// <summary>
        /// Used to designate an actor gameobject
        /// </summary>
        [XmlAttribute("target")]
        public string target = String.Empty;

        /// <summary>
        /// Used to designate an actor's name. Used for Textboxes
        /// </summary>
        [XmlAttribute("character")]
        public string character = String.Empty;

        /// <summary>
        /// Used to designate type
        /// </summary>
        [XmlAttribute("type")]
        public string type = String.Empty;

        /// <summary>
        /// Used for storing strings for messages
        /// </summary>
        [XmlAttribute("message")]
        public string message = String.Empty;

        /// <summary>
        /// Used to determine speed
        /// </summary>
        [XmlAttribute("speed")]
        public float speed;

        /// <summary>
        /// Used for Counters
        /// </summary>
        [XmlAttribute("duration")]
        public float duration;

        /// <summary>
        /// Used to choose the image associated with this command
        /// </summary>
        [XmlAttribute("image")]
        public string image = String.Empty;

        /// <summary>
        /// Used for extra/misc parameters (offsets, etc)
        /// </summary>
        [XmlAttribute("extra")]
        public string extra = String.Empty;
    }
}