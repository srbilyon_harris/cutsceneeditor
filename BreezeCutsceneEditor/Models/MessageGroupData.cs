﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BreezeCutsceneEditor
{
    [XmlRoot("MessageGroups")]
    [Serializable()]
    public class MessageGroupData
    {
        [XmlArray("Messages")]
        [XmlArrayItem("Message")]
        public List<MessageData> Messages = new List<MessageData>();
    }
}
