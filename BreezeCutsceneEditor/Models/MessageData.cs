﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace BreezeCutsceneEditor
{
    [System.Serializable]
    public class MessageData
    {
        //[XmlArray("Lines")]
        //[XmlArrayItem("line")]
        //public List<LineData> lines = new List<LineData>();

        //message id
        [XmlAttribute("id")]
        public int id;

        [XmlAttribute("messageGroupID")]
        public int messageGroupID;

        [XmlAttribute("message")]
        public string message;
    }
}
