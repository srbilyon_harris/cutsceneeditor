﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BreezeCutsceneEditor
{
    [XmlRoot("EventCommands")]
    [Serializable()]
    public class CutsceneData
    {
        [XmlArray("Commands")]
        [XmlArrayItem("Command")]
        public List<CommandData> CommandData = new List<CommandData>();
    }
}
